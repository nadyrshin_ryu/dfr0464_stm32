//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_i2c.h>
#include <aip31068.h>
#include <i2cm.h>


/*
//==============================================================================
// ������� ��������� ��������� ����� pcf8574. ���������� ��������� �������� ��� 0 - � ������ ������� 
//==============================================================================
uint8_t pcf8574_read(I2C_TypeDef* I2Cx, uint8_t slave_addr)
{
  uint8_t val = 0;
  
  // ����� START �� ����
  if (i2cm_Start(I2Cx, slave_addr, 1, PCF8574_TO))
  {
    i2cm_Stop(I2Cx, PCF8574_TO);
    pcf8574_bus_init(I2Cx);
    return 0;
  }
  // ������ 1 ����
  if (i2cm_ReadBuffAndStop(I2Cx, &val, 1, PCF8574_TO))
    return 0;
  
  return val;
}
//==============================================================================


//==============================================================================
// ������� ��������� ��������� ����� pcf8574 � ����� buff. ���������� ��� ������ (0 - � ������ ������) 
//==============================================================================
uint8_t pcf8574_read_ex(I2C_TypeDef* I2Cx, uint8_t slave_addr, uint8_t *buff)
{
  int8_t err;
  
  // ����� START �� ����
  err = i2cm_Start(I2Cx, slave_addr, 1, PCF8574_TO);
  if (err)
  {
    i2cm_Stop(I2Cx, PCF8574_TO);
    pcf8574_bus_init(I2Cx);
    return err;
  }
  // ������ 1 ����
  err = i2cm_ReadBuffAndStop(I2Cx, buff, 1, PCF8574_TO);
  if (err)
    return err;
  
  return I2C_ERR_Ok;
}
//==============================================================================


//==============================================================================
// ������� ���������� ��������� ����� pcf8574 �� ��������� value. ���������� ��� ������ (0 - � ������ ������) 
//==============================================================================
uint8_t pcf8574_write(I2C_TypeDef* I2Cx, uint8_t slave_addr, uint8_t value)
{
  int8_t err;
  
  // ����� START �� ����
  err = i2cm_Start(I2Cx, slave_addr, 0, PCF8574_TO);
  if (err)
  {
    i2cm_Stop(I2Cx, PCF8574_TO);
    pcf8574_bus_init(I2Cx);
    return err;
  }
  
  err = i2cm_WriteBuff(I2Cx, &value, 1, PCF8574_TO);
  i2cm_Stop(I2Cx, PCF8574_TO);
  if (err)
    return err;
  
  return I2C_ERR_Ok;
}
//==============================================================================
*/

//==============================================================================
// ������������� i2c ��� ������ � pcf8574
//==============================================================================
void aip31068_init(I2C_TypeDef* I2Cx)
{
  uint8_t FuncFlags = 0;
  
#if (AIP31068_ROWS > 1) 
  FuncFlags |= AIP31068_FUNC_2LINE;
#endif
        
  _numlines = AIP31068_ROWS;
  _currline = 0;

  // for some 1 line displays you can select a 10 pixel high font
#if (AIP31068_FONT == AIP31068_FONT_5x8)
  FuncFlags |= LCD_5x10DOTS;
#endif

  // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
  // according to datasheet, we need at least 40ms after power rises above 2.7V before sending commands
  delay_ms(50);

  // this is according to the hitachi HD44780 datasheet page 45 figure 23
  // Send function set command sequence
  command(LCD_FUNCTIONSET | FuncFlags);
  delay(5);  // wait more than 4.1ms
	
  // second try
  command(LCD_FUNCTIONSET | FuncFlags);
  delay(5);

  // third go
  command(LCD_FUNCTIONSET | FuncFlags);




    ///< turn the display on with no cursor or blinking default
    _showcontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
    display();

    ///< clear it off
    clear();

    ///< Initialize to default text direction (for romance languages)
    _showmode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
    ///< set the entry mode
    command(LCD_ENTRYMODESET | _showmode);
    
    
    ///< backlight init
    setReg(REG_MODE1, 0);
    ///< set LEDs controllable by both PWM and GRPPWM registers
    setReg(REG_OUTPUT, 0xFF);
    ///< set MODE2 values
    ///< 0010 0000 -> 0x20  (DMBLNK to 1, ie blinky mode)
    setReg(REG_MODE2, 0x20);
    
    setColorWhite();

}
//==============================================================================
*/
