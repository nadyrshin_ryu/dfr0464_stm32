//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _PCF8574_H
#define _PCF8574_H

#include <stdint.h>


#define AIP31068_I2C_ADDR       0x7C    // i2c-����� ����������

// ������� ����������
#define AIP31068_ROWS           2       // ���-�� �����
#define AIP31068_COLS           16      // ���-�� ��������

#define AIP31068_FONT_5x8       0
#define AIP31068_FONT_5x10      1
#define AIP31068_FONT           AIP31068_FONT_5x8



// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define AIP31068_FUNC_8BITMODE  0x10
#define AIP31068_FUNC_2LINE     0x08
#define AIP31068_FUNC_5x10DOTS  0x04


/*
#define PCF8574_i2cRate         100000  // ������� ������ i2c ��� ������ � pcf8574
#define PCF8574_TO              1000    // ������������ �������� �������� �������� �� i2c ��� ������ � pcf8574


// ������� ��������� ��������� ����� pcf8574. ���������� ��������� �������� ��� 0 - � ������ ������� 
uint8_t pcf8574_read(uint8_t slave_addr);
// ������� ��������� ��������� ����� pcf8574 � ����� buff. ���������� ��� ������ (0 - � ������ ������) 
uint8_t pcf8574_read_ex(uint8_t slave_addr, uint8_t *buff);
// ������� ���������� ��������� ����� pcf8574 �� ��������� value. ���������� ��� ������ (0 - � ������ ������) 
uint8_t pcf8574_write(uint8_t slave_addr, uint8_t value);
// ������������� i2c ��� ������ � pcf8574
void pcf8574_bus_init(void);
*/

#endif